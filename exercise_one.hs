double x = x * 2

evens x = [i | i <- [1..x], even i]

capitalized z = elem (z !! 0) ['A'..'Z']

abbreviate y = [(head y), (last y)]

powers2 n = [2^i | i <- [1..n]]

powers2' 0 = []
powers2' n = (powers2' (n-1)) ++ [(2 ^ n)]
