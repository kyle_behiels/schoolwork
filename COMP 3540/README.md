# Human Computer Interaction

**Instructor** : Piper Jackson

Moodle : yellow noun

## Evaluation

- Attendance 5%
- Seminar Exercises 5%
- Quizzes 10%
- Team Project 25% - Groups of ~4
- Midterm Exam 20%
- Final Exam 35%

Minimum 50% on both exam and non-exam portions. 90% mandatory attendance.

---

