import random

arrival_rate = 0.0
departure_rate = 0.7
sim_time = 1000000
results = [0]*10


def run_simulation():
    for i in range(0, 10):
        queue_length = 0
        for x in range(0, sim_time):
            if packet_arrived(arrival_rate):
                queue_length += 1
            if packet_departed(departure_rate):
                queue_length -= 1


def packet_arrived(probability):
    int_prob = int(probability*10)
    return random.randint(0, 10) < int_prob
        


def packet_departed(probability):
    int_prob = int(probability*10)
    return random.randint(0, 10) < int_prob
        



