import random
import queue
import time

FIDELITY_OFFSET = 100
event_scheduler = queue.Queue()
HEADER = 54                         #54 Byte header size
PACKET_LENGTH = 1500                #1500 Byte packet length
LOSS_THRESHOLD = 5

ber_set = [0, 1e-4, 1e-5]
tau_set = [5, 250]
timeout_set = [2.5, 5, 7.5, 10, 12.5]

# After a forward channel event is registered, purge and reset timeout at tc + L/C
# A new timeout must be registered at tc + L/C + time_out

class event:

    def __init__(self, time, event_type, time_out, has_error, sequence_number):
        self.time = time
        self.event_type = event_type
        self.has_error = has_error
        self.sequence_number = sequence_number
        self.time_out = time_out

class frame:

    def __init__(self, sequence_number, packet_length):
        self.sequence_number = sequence_number
        self.packet_length = packet_length

class abp_sender:

    def __init__(self,_time_out, mChannel, mReceiver):
        self.time_out = _time_out
        self.mChannel = mChannel
        self.mReceiver = mReceiver
        self.current_time = 0
        self.next_expected_ack = 1
        self.sequence_number = 0

        self.successfully_sent_packets = 0
        self.total = 0
        self.retransmissions = 0

    def SEND(self, success_packet_count):
        while (self.successfully_sent_packets < success_packet_count):
            # Generate packet
            length = HEADER + PACKET_LENGTH
            trans_delay = int((float(length) / float(self.mChannel.channel_capacity))*FIDELITY_OFFSET)
            current_frame = frame(self.sequence_number, length)

            # Frame has been transferred, register an event
            eventTimeOut = event(self.current_time, "TIMEOUT", (self.current_time + trans_delay + self.time_out), None, None)

            event_scheduler.put(eventTimeOut)
            sort_queue_by_time()

            #Forward channel implementation
            self.current_time = self.current_time + (trans_delay)
            packet_transfer_status = self.mChannel.getPacketState(current_frame.packet_length, LOSS_THRESHOLD) # Five is an arbitrary number that represents how many packets can be lost befor the frame is lost

            if(packet_transfer_status == "ERROR"):
                event_scheduler.put(event(self.current_time, "PACKET_TRANSFER", None, True, self.sequence_number))
            else:
                event_scheduler.put(event(self.current_time, "PACKET_TRANSFER", None, False, self.sequence_number))
            sort_queue_by_time()

            # NOTE: Receiver will register an ack in the event scheduler only if the amount of bits lost is below the LOSS_THRESHOLD
            # Reverse channel implementation
            self.mReceiver.receive(current_frame, self.current_time)
            ack_received = event_scheduler.get()

            if(ack_received.event_type == "TIMEOUT"):
                self.retransmissions += 1
                self.current_time += ack_received.time_out

            else:
                #Check if ack was received before TIMEOUT & verify that the next element was a timeout otherwise throw an exception
                timeout_event = event_scheduler.get()

                if(ack_received.time < timeout_event.time_out): # Ack was received on time
                    if(ack_received.event_type == "ACK_ERROR"):     #Packet arrived at receiver w/ error, retransmit
                        self.retransmissions += 1
                        self.current_time += 2*self.mChannel.propogation_delay
                    elif(ack_received.event_type == "ACK_NOERROR" and ack_received.sequence_number == self.next_expected_ack):
                        # Moving on to the next packed is simulated by incrementing sequence_number and next_expected_ack. NOTE: Both use mod2 arithmetic
                        self.sequence_number = (self.sequence_number + 1) % 2
                        self.next_expected_ack = (self.next_expected_ack + 1) % 2
                        self.current_time += 2*self.mChannel.propogation_delay
                        self.successfully_sent_packets = self.successfully_sent_packets + 1
                    else:
                        self.retransmissions += 1
                        self.current_time += 2*self.mChannel.propogation_delay
        return self.current_time

class channel:
    channel_capacity = 500000/FIDELITY_OFFSET     #5Mb/s channel capacity = 5kb/ms

    def __init__(self,_propogation_delay, _ploss, _bit_error_rate):
        self.propogation_delay = _propogation_delay
        self.p_loss = _ploss
        self.bit_error_rate = _bit_error_rate

    # Check if a precision of 10000 is necessary. Could very well slow down the simulation.
    def binary_bernoulli(self, probability):
        result = random.randint(0,100000)
        if result < int(probability * 100000):
            return True
        else:
            return False
    def getPacketState(self, length, loss_threshold):
        error_count = 0
        success_count = 0

        for i in range(0, length):
            if self.binary_bernoulli(self.bit_error_rate):
                error_count += 1
            else:
                success_count += 1
        if error_count > loss_threshold:
            return "LOSS"
        elif (error_count > 0) and (error_count < loss_threshold):
            return "ERROR"
        else:
            return "NOERROR"

class abp_receiver:

    def __init__(self, channel):
        self.next_expected_frame = 0
        self.current_time = 0
        self.channel = channel
    def receive(self, frame, current_time):
        current_event = event_scheduler.get()

        if(current_event.event_type == "TIMEOUT"):
            event_scheduler.put(current_event)
            sort_queue_by_time()
            return

        # Run a simulated channel to check if the ack is lost
        ack_status = self.channel.getPacketState(54, LOSS_THRESHOLD)
        ack_has_error = None
        if(ack_status == "ERROR"):
            # ack has error, set packet_ack to error
            ack_has_error = True

        # ack is lost completely. Do nothing

        elif(ack_status == "NOERROR"):
            ack_has_error = False

        if(current_event.has_error):
            # This will be triggered when an error is put into event schedule"
            event_scheduler.put(event(current_time + int(54.0/(float(channel.channel_capacity))*FIDELITY_OFFSET), "ACK_ERROR", None, ack_has_error, (current_event.sequence_number+1)%2 ))
        elif(not current_event.has_error):
            event_scheduler.put(event(current_time + int(54.0/(float(channel.channel_capacity))*FIDELITY_OFFSET), "ACK_NOERROR", None, ack_has_error, (current_event.sequence_number+1)%2 ))
        sort_queue_by_time()


def sort_queue_by_time():

    mlist = list(event_scheduler.queue)
    mlist.sort(key=lambda event: event.time, reverse=True )
    event_scheduler.queue.clear()
    for element in mlist:
        event_scheduler.put(element)

def main():
    seperator_thick = "==============================================================="
    seperator_thin = "---------------------------------------------------------------"
    start_time = int(round(time.time() * 1000))
    for tau in tau_set:
        for ber in ber_set:
            for timeout in timeout_set:
                myChannel = channel(tau, 0, ber)
                myReceiver = abp_receiver(myChannel)
                mySender = abp_sender(2*tau + timeout, myChannel, myReceiver )
                time_taken = mySender.SEND(10000)
                print (seperator_thick)
                print ("Successful transmissions = ", mySender.successfully_sent_packets, " | Retransmissions = ", mySender.retransmissions)
                print (seperator_thin)
                print ("Finished sim for tau = ", tau, " ber = ", ber, "timeout = ",timeout, " in ", time_taken)
                print (seperator_thin)
                print ("THROUGHPUT = ", round(10000/(time_taken/FIDELITY_OFFSET), 3), "bits/millesecond or ", round((10000/(time_taken/FIDELITY_OFFSET))*1000, 3), " bps")
                print (seperator_thick)


main()
