#include <stdio.h>

int first, second, third;

int main(){
	

	printf("Please enter your first number:\n");
	scanf("%d", &first);
	
	printf("Please enter your second number:\n");
	scanf("%d", &second);

	printf("Please enter your third number:\n");
	scanf("%d", &third);

	if((first == second) || (second == third) || (first == third)){
		printf("No two numbers can be the same\n");
		return 1;
	}

	int biggest = first > second ? (first > third ? first : third) : (second > third ? second : third);
	
	printf("%d is the biggest number you entered\n", biggest);

	return 0;

	
}


