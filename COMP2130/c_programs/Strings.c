#include <stdio.h>
#include <string.h>

int isEqual(char*, char*);
void convertToUpper(char*);
void convertToLower(char*);
void displayString(char*);


void convertName(char *mName){
	if(mName[0] < 97){
		mName[0] = mName[0] + 32;
	}
	for(int i = 1; i < strlen(mName); i++){
		if(mName[i] == ' '){
			if(mName[i+1] < 97){
				mName[i+1] = mName[i+1]+32;
			}
		}
	}
}


int main(){
	char mString[] = "kyle behiels";
	
	convertName(&mString);
	
	displayString(mString);
	
	
	return 0;
}

void displayString(char* mString){
	printf("%s\n", mString);
}

int isEqual(char* firstString, char* secondString){
	for(int i = 0; i < strlen(firstString); i++){
		if(!(firstString[i] == secondString[i])){
			// Not equal
			return 1;
		}
	}
	// Equal
	return 0;
}


