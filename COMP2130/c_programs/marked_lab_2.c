#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int strLength(char[]);

// Number to check and bitshift val
int countTrailingZeros();

void countChars(char str[]);

int convertToBinary(long);

int shouldContinue = 1;
char mString;

int main(){
	
	while(shouldContinue == 1){
		int choice;
		printf("What is your string?\n");
		scanf("%s", &mString);
		printf("Main Menu\n============\n");
		printf("0 - Exit\n1 - Get string length\n2 - Count trailing zeros\n3 - Count characters\n");
		scanf("%d", &choice);
		switch(choice){
			case 1:
				printf("Your string is %d characters long.\n", strLength(&mString));
				break;
			case 2:
				printf("Total trailing zeros is %d\n", countTrailingZeros());
				printf("Exited trailing\n");
				break;
			case 3:
				countChars(&mString);
				break;
			default:
				printf("Invalid option, try again.");
			}
		printf("Go again? 1 - yes, 0 - no\n");
		scanf("%d", &shouldContinue);
		}
	return 0;
	}
	
int strLength(char str[]){
	int total = 0;
	while(str[total] != '\0'){
		total++;
		}
	return total;
	}
	
int countTrailingZeros(){
	int number;
	int bitShift;
	int count = 0;
	
	printf("What is your number?\n");
	scanf("%d", &number);
	printf("What is your bitshift to the left?\n");
	scanf("%d", &bitShift);
	printf("pre shift %d\n", convertToBinary(number));
	number = number << bitShift;
	printf("post shift%d\n", convertToBinary(number));
	
	while (number != 0)
    {
        if ((number & 1) == 1)
        {
            break;
        }
        else
        {
            count++;
            number = number >> 1;
        }
    }
	return count;
}
void countChars(char str[]){
	
	int countArray[26];
	for(int x = 0; x<26; x++){
		countArray[x] = 0;
	}
	char current;
	int i = 0;
	while(str[i] != '\0'){
		current = str[i];
		if(current > 96){
			current = current - 32;
		}
		current = current - 65;
		countArray[(int)current]++;
		i++;
	}
	for(int z = 0; z<26; z++){
		if(countArray[z] > 0){
			printf("%c found %d times\n", (z+65), countArray[z]);
		}
	}
	
}	

int convertToBinary(long mNumber){
	int remainder;
	
	long binary = 0, i = 1;
	while(mNumber != 0){
		remainder = mNumber%2;
		mNumber = mNumber/2;
		binary = binary + (remainder*i);
		i = i * 10;
		}
	return binary;
	
}

