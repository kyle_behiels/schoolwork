#include <stdio.h>
#include <math.h>

float mNumber;

int main(){
	
	printf("Please enter a number to be checked\n");

	scanf("%f", &mNumber);
	
	if((mNumber / 2.0) == (float)round(mNumber / 2.0)){
		printf("Your number was even!\n");
	}
	else{
		printf("Your number was odd!\n");
	}
}
