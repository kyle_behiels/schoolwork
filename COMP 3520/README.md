# Software Engineering

**Instructor** : Kevin O'Neil

**Textbook** : Sommerville, Ian Software Engineering, 10th Edition (Not Required)

## Evaluation

Project / Assignments -> 25%

Midterm **Thursday Oct 18** -> 25%

Final Exam -> 50%

* * *

# Notes

Process -> A way of doing things

## Chapter One - The software process

-   Structured set of activities
    		\- Specification
    		\- Design and Implementation
    		\- Validation
    		\- Evolution

A software process is an abstract representation of a process. It represents a description of a process from some particular perspective

When describing a process we usually talk about the activities in these processes like data model, UI

-   Products (outcomes)
-   Roles
-   Pre and post conditions

Plan driven processes are processes where all activites are planned in advance
Agile - Incremental and easier to change the process to reflect changing requirements

Waterfall Model - Plan-driven model. Separate and distinct phases of specification and development

Incremental Development - Specification, development and validation are interleaved. May be plan-driven or agile.

Integration and configuration - The system is assembled from existing configurable components. May be plan-driven or agile

In practice most companies implement a combination

### Waterfall model phases

-   Requirement analysis and definition
-   System and software design
-   Implementation and unit testing
-   Integration and system testing
-   Operation and maintenance

Inflexible partitioning of the project into distinct stages makes it difficult to respond to changing customer requirements

Used mainly for large systems engineering projects where a system is developed at several sites

Hard Problem - Generally not solvable. Conflicting requirements/ prohibitively expensive

### Incremental Development

The cost of accomadating changing customer requirements is reduced

It is easier to get customer feedback on the development work that has been done

More reapid delivery and deployment of useful software to the customer possible

### Incremental Development Process

-   The process is not visible
    		\- Managers need regular deliverables to measure progress
-   Structure tends to degrade as new increments are added
    		\- Unless time and money is spent on refractoring to improve the software, regular change tends to corrupt it's structure

### Integration and Configuration

-   Based on existing software reuse where systems are integrated from existing components
-   Reused elements may be configured to adapt their behaviour and functionality to a user's requirements

Stand alone application systems -> (sometimes called COTS) that are configured for use in a particular environment

Collections of objects -> Developed as a package to be integrated with a component framework

Web services -> Developed according to service standards and which are available for remote invocation

### Key Process Stages

-   Requirement specification
-   Software discovery and evaluation
-   Requirement specification
-   Application system configuration
-   Component adaptation and integration

### Advantages and Disadvantages

-   Reduced cost and risks as less software is developed from scratch
-   Faster delivery and deployment of system
-   But requirements compromises are inevitable so system may not meet real needs of users
-   Loss of control over evolution of reused system elements

### Process activities

-   Interleaved sequences of technical, collaborative and managerial activities with the overall goal of specifying, designing, implementing and testing a software system
-   The four basic process activities of specification, development, validation and evolution are organized differently in different development processes
-   Waterfall model -> organized in sequence, whereas in incremental development they are interleaved

### Software Specifications

-   Process of establishing what services are required and the constraints on the systems operation and development
-   Requirements engineering process

### Design and Implementation

-   Converting the system spec into an executable system
-   Design is a structure that realises the spec
-   Implementation - Translate structure into an executable
-   Design and implementation are closely interleaved

### Design Activities

-   **Architectural Design** - Identify the overall structure of the system
-   **Database Design** - Design the system data structure
-   **Interface Design** - Define the interfaces between system components

### Implementation

-   Implemented by developing or implementing existing components
-   Design and implementation are interleaved
-   Programming is an individual activity with no standard process
-   **Debugging** - Look for faults and correcting them

### Validation

-   **Verification and validation** - intended to show that a system conforms to its spec
-   Checking and review processes
-   **System Testing** - Involves executing the system with test cases that are derived from the spec

### Testing stages

-   Component testing - Test components individually
-   System Testing - Test the system as a whole
-   Customer testing - Test w/ customer data to check that the system meets the customers needs

## Software Evolution

-   Software is inherently flexible and can change
-   Requirements change through changing circumstances

### Coping with change

-   Change is inevitable in all large software projects
    -   Business changes lead to new and changed system requirements
    -   New technologies open up new possibilities for improving implementations
    -   Changing platforms require application changes
-   Change leads to rework so the costs of change include both rework as well as the costs of implementing new functionality

### Reducing the cost of rework

-   Change anticipation where the software process includes activities that can anticipate possible changes before significant rework is required
-   Develop for change tolerance. Meaning changes can be made for cheap.

### Coping with changing requirements

-   Prototyping where a version of the system is developed quickly to check the customers requirements and the feasibility of design decisions.
-   Incremental delivery, where system increments are delivered to the customer for comment and experimentation. This supports both change avoidance and change tolerance.

### Software prototyping

-   Prototype is an initial version of a system used to demonstrate concepts
    -   The requirements engineering process to help with requirements

\| **Benefits** \|
\|---\|
| Improved System Usability |
| Closer fit to user needs |
| Improved Quality |

### Prototype Development

-   May be based on rapid prototyping languages or tools
-   May invlove leaving out functionality
    -   Should focus on ares of the product that are not well understood
    -   Error checking and recovery
    -   Functional rather than non-functional requirements

### Throw-away Prototypes

-   Prototypes should be discarded after development as they are not a good basis for a production system
    -   May be impossible to tune the system to meet non-functional requirements
    -   Normally undocumented
    -   degrades

### Incremental Delivery

-   Development and delivery is broken down into increments with each increment delivering part of the required functionality
-   Requirements are prioritised and the highest priority reqs are included in early increments
-   Once the development of an increment is started, the requirements are frozen through requirements so later increments can continue to evolve

### Incremental development and delivery

-   Development
    -   Develop the system in increments and evaluate each increment
    -   Normal approach used in agile methods
    -   Evaluation done by customer
-   Incremental Delivery
    -   Deploy an increment for use by end users
    -   More realistic evaluation about practical use of software

## Process Improvement

Changes to the process that enable faster development

### Approaches to improvement

-   The process maturity approach, which focuses on improving process management and introducing good software engineering practice
-   The agile approach, which focuses on iterative development and the reduction of overheads in the software process

### Activities

-   **Measurement** - Where possible quantitative data should be collected
    -   Process metrics -> Time taken for process activities to be completed
    -   Resources required for processes or activities
    -   Number of occurrences of a particular event

### SEI Capability Maturity Model

-   Initial
-   Repeatable
-   Defined
-   Managed
-   Optimising

## Agile Development

Rapid Software Development

-   Rapid development and delivery  is now often the most important requirement
-   Plan driven development is essential for some types of system but does not meet these business needs
-   Agile development methods emerged in the late 1990s whose aim was to radically reduce the delivery time for working software systems

Agile Development

-   Program specification design and implementation are interleaved
-   System is developed as a series of versions or increments with stakeholders involved in version spec and evaluation
-   Frequent delivery of new versions for evaluation
-   Extensive tool support
-   Minimal Documentation - focus on working code

**Customer involvement** - Immediately necessary, any delays are expensive and cause delays

**MVP** - Minimal Viable Product -> Least amount of work that can be sold to the customer

### Agile method applicability

-   Product development where companies are small and low on resources

## Extreme programming

-   Several builds a day
-   Increments are delivered every couple of weeks

### Scrum terminology

**Scrum** - Daily meeting of the scrum team that reviews progress and prioritizes work to be done that day. Ideally, this should be a short face to face meeting

ScrumManager - Meeting leader, not necessarily the team manager

Sprint - A development iteration

Velocity - How fast we are getting work done / How much can we get done in any increment

- **Sprints** are fixed length normally 2-4 weeks
- Starting point for planning is the product backlog which is the list of work to be done on the project
- Team is self organizing
  
### Organizational Issues

- Traditional engineering organizations have a culture of plan-based development
- Will the customer be available

**Agile methods for larger systems**

- Large systems usually integrate and interact with many other systems. 
- Teams work in different zones 
- Most of the work is integration rather than development

**Agile methods across organizations**

- Project managers who do not have experiences of agile methods may be reluctant to accept the risk of a new approach
- Large organizations often have quality procedures and standards that all projects are expected to follow.
- Agile works best with skilled individuals
- There might be a cultural resistance to agile methods

**Key Points**

- Agile Methods are incremental development methods that focus on rapid software development, frequent releases of software, reducing process overheads by minimizing documentation and producign quality code
- Scrum is an agile method that provides a project management framework
- Many practical development methods are a mixture of plan-based and agile development
- Scaling agile memthods for large systems is difficult

# Software Testing

## Program Testing

- **Testing** is intended to show that a program does what is intended
- Uses artificial data to run software
- Check results of test for run errors, anomolies or inconsistency
- Testing is part of a more general verification and validation process

### Goals

- Demonstrate that the developer and the customer that the software meets its requirements
- Discover situations in which the behaviour of the software is incorrect, undesirable or does not conform to its spec

## Validation and defect testing

- First goal leads to validation testing
- The second goal leads to defect testing

**Validation** - Does what it is supposed to do

**Defect Testing** - Discover strange cases that break the application

### Software inspections
- Involves people examining the source 
- Inspections do not require the execution of a system
- During testing, errors cna mask other errors
- Incomplete versions of a system can be inspected without extra cost
- Can be considered a broader quality of attributes of a program

Both testing and inspections should be used during the V & V process

### Stages of testing

- Development testing - Developers test as the software is being built
- Release testing - Where a seperate team test a complete version of the system before it is released to users
- User testing - Where users or potential users of a system test the system in their own environment
  
**Development Testing** 
- Unit testing - Individual program units or object classes
    - Test utens ub usikatuib
    - It is a defect testing process
- Component testing - Software components
- System testing - The entire unit

### Testing Guidelines

- Test to the extreme ends of your use case
- Always test pointer parameters with null pointers
- Design tests which cause the component to fail
- Use stress testing in message passing systems
- In shared memory systems, vary the order in which components are activated

### System Testing

- Developemt involves integrating components
- Test the interactions between the components in the system
- Gives us the emergent behaviour of the system

### System and component testing

- System is assembled and then tested
- Components developed by different team members or sub teams may be integrated at this stage. Collective rather than individual

### Use case testing

- The use-cases developed to identify system interactions can be used as a basis for system testing
- Each use case involves several system components
- Sequence diagrams associated w/ the use case

### Testing policies

- Exhaustive system testing is impossible so testing policies which define the required system test coverage may be developed
- Examples:
    - All system functions that are accessed through menus should be tested
    - Combinations of functions that are accessed through the same menu should be tested
    - etc...
  
## Test Driven Development

- Write a little bit of code and test it. Write code incrementally and then test the code an increment at a time.
- Before we write the code, write the test case