# Python script for scraping a single table off of a website.
# ===================================================================================
# METHODS
# -----------------------------------------------------------------------------------
# read_table(table_page, parent_class, parent_id)
# table_page = String
# -- Reads a table from a webpage and generates a 2D array that represents the table.
# NOTE: Pass a table gotten from the array output of getAllTables
#-------------------------------------------------------------------------------------
# gen_csv_output(twoDArray, heading)
# twoDArray = Two d array generated from read_table
# heading = bool
# -- Takes a two dimensional array as a parameter and outputs a string that when piped
# 	 to a CSV file is fully ready for import into excel, weka etc...
# NOTE: Heading is only used when creating a new file. If you are using a linux pipe
# 		and wish to append the data to a csv file leave heading=false
#======================================================================================
# USAGE
# -------------------------------------------------------------------------------------
# NOTE: Use read table to create a 2D array. This 2D array can then be used as a
# parameter for gen_csv_output. This is designed so that you can print the output of
# gen_csv_output to console and pipe the results to a CSV file
# ***Depending on the format of the table, extra processing might be necessary on
# the two dimensional array
# -------------------------------------------------------------------------------------
# TO-DO: Be able to pass flags to program?

from bs4 import BeautifulSoup as bs
import urllib3

http = urllib3.PoolManager()

sep_lg = "============================================================================"
sep_sm = "----------------------------------------------------------------------------"
sep_sld = "____________________________________________________________________________"


# If there is more than one table on a web page, there needs to be the option
# to choose a particular table
# -----------------------------------------------------------------------------
# Verbosity flag set to true will print a preview of the tables before return
def getAllTables(table_page, verbose=False):
	url = table_page
	webpage = http.request('GET', url)

	soup = bs(webpage.data, "html.parser")
	tables = []

	for table in soup.find_all('table'):
		tables.append(table)
	if(verbose):
		print(sep_lg)
		print("Found ", len(tables)," tables.")
		print(sep_lg)
		for i in range(0, len(tables)):
			table_data = tables[i].findChildren()
			print("TABLE #",i, "(This will show the first row in table)")
			print(sep_sm)
			print(table_data[0])
			print(sep_sm)

	return tables


# Returns a 2D array with the scraped tables values
# position 0 = a list of table headings
# every position after that is a row
#=============================================================================
# NOTE: If there is missing data (row length != cardinality) there will either
# be an error or the table will be read innaccurrately
def read_table(table):

	_table = table
	if(parent_id == "none"):
		table_heads = []

		for th in _table.find_all('th'):
			table_heads.append(th.get_text())

		categories = len(table_heads)

		table_data = []

		for td in _table.find_all('td'):
			tempData = td.get_text()
			tempData = tempData.replace(" ", "")
			tempData = tempData.replace("\n", "")
			if(tempData[0] == '.'):
				tempData = "0"+tempData
			table_data.append(tempData)

	else:
		table_heads = []

		for th in _table.find(id=parent_id).find_all('th'):
			table_heads.append(th.get_text())

		categories = len(table_heads)

		table_data = []

		for td in _table.find(id=parent_id).find_all('td'):
			tempData = td.get_text()
			tempData = tempData.replace(" ", "")
			tempData = tempData.replace("\n", "")
			if(tempData[0] == '.'):
				tempData = "0"+tempData
			table_data.append(tempData)

	total_entries = int(len(table_data) / categories)

	sorted_data = []

	sorted_data.append(table_heads)

	for i in range(0, total_entries):
		temp_array = []
		for x in range(0, categories):
			temp_array.append(table_data[(i*categories)+x])
		sorted_data.append(temp_array)

	return sorted_data

#======================================================================
# NOTE: Use the headings=True flag to include the headings in the CSV
# (not yet finished)
#======================================================================
def gen_csv_output(twoDArray, heading=False):
	returnable=""
	if(heading):
		for i in range(0, len(twoDArray[0])):
			returnable = returnable + ("\"" + twoDArray[0][i] + "\",")
	returnable = returnable[:-1]
	returnable = returnable + "\n"
	for i in range(1, len(twoDArray)):
		for x in range(0, len(twoDArray[i])):
			if(twoDArray[i][x].isalpha()):
				returnable = returnable + ("\""+twoDArray[i][x]+"\""+',')
			else:
				returnable = (returnable + twoDArray[i][x]+',')

		returnable = returnable[:-1]
		returnable = returnable + '\n'
	returnable=returnable[:-1]
	return returnable


# Main method for organizing outputs
def main():
	done = False
	while (!done)
		print("Please enter the url to scrape")
		

main()
