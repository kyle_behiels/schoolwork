import random, queue

# Bernoulli(n, p) where n is num trials and
# p is probability of success

lambda_set = [0.2, 0.3, 0.4,0.45,0.49,0.495]
servers = [0, 0, 0, 0, 0]
jobs = queue.Queue()
server_count = 5
SIM_LENGTH = 1000

def job():
    jobId = ""
    def __init__(self):
        jobId = self.jobId
    timeAlive = 0

def server():
    jobQueue


def main():
    for i in range(0, SIM_LENGTH):
        for lam in lambda_set:

            # Deal with arrivals
            arrivedJobs = getBernoulli(server_count, lam)
            for x in range(0, arrivedJobs):
                servers[servers.index(min(servers))]+=1
                jobs.put({"timeAlive": 0})  
            print("Running")
            # Deal with departures


        for job in iter(jobs.get, None):
            job["timeAlive"] += 1
            print("running")
    print(jobs)

def getBernoulli(n, p):
    successes = 0
    for i in range(0,n):
        result = random.randint(1,10)/10
        if(result <= p):
            successes+=1
    return successes



main()