

def bernoulli(n,p)
	successes = 0
	n.times {
		if (rand(10)/10) < p
		       successes += 1
		end
	}
	return successes
end

10.times{ |x|
	puts bernoulli(x, 0.5)
}

