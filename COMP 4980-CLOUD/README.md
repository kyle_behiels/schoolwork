# Cloud Computing

**Instructor** : Ning Lu

Office: OL 411

Textbook: All Reccommended 

- Perormance Modeling and Design of Computer Systems: Queueing Theory
- Communication Networks: An Optimization, Control and Stochastic Perspective

## Evaluation

- 20% Bi-weekly Quiz
- 20% Theory assignments
- 30% Programming assignments
- 30% Final Exam

## Software

MapReduce/Spark

---

## Notes

**Cloud** - Data center (networking)

**Computing** - Big-Data applications (algorithms)

Internet vs. Data Center Network

- Internet -> Connection of servers, user devices and switches.  Multi-layered.
- Data Center -> Switches and routers to connect all of the servers.

Computing as a utility - Economies of scale and Emergence of big data

- Large, centralized servers providing service for many clients
- Cost per unit significantly reduced
- Scales to huge computing power if necessary

## Load Balancing

Goal: Minimize time to respond to request

Load balancing algorithms ideally simple but balance reasonably well

## Networking

Goal: Provide means for servers to communicate

Probability, optimization, graph theory for load balancing and networking

## Other

Virtual machine placement, Data storage, Data locality, etc...

## Math

- Probability
- Markov Chain
- Queueing Theory
- Linear Algebra
- Linear Programming
- Convex Optimization

<<<<<<< Updated upstream
## Introduction to Cloud Computing

Cloud Computing - Model for enabling convenient on-demand network access to a shared pool of configurable computing resources.

### On-demand self-service

A consumer can unilaterally provision capabilities without need of admin interaction

Available over network

Accessed through standard mechanisms
=======
## Queueing Theory

**Queueing theory** - Useful when there are lots of jobs and limited resources
> The theory of queues

Arriving Jobs

| 1 | 2 | 3 | 4 | -> (SERVER)

### Computer system examples

- CPU Scheduling 
- Hard Disk -> Reading and writing blocks from a hard disk
- Router in a computer network -> Routing packets 

### Goals

1. Predicting the system performance
    - Mean queue length -> Decide how large a buffer is necessary
    - Mean Delay -> Important to determine waiting time for clients/jobs 
    - Utilization -> Decide how much service to provide for clients/jobs
2. System design and planning
    - Additional resources -> Balance cost and reliability
    - Smarter scheduler

### Examples 

**Design example one** - Doubling arrival rate

FCFS Queue -> CPU

service rate = <MU> = 5 

Meaning the cpu will serve 5 jobs/sec

Queue is populated at a rate of <LAMBDA> = 3 

Meaning the queue will grow by 3 every second

According to some random process. If this example is not random, there will never be a queueing delay.

**Metric**: Mean Response Time E[T] 

Response Time - The time from when a job arrives until it completes it's service (AKA Sojourn time)

**Question**

Arrival rate will double starting tommorow. You are tasked with upgrading this system. We want to have the same mean response time when the arrival time doubles. By how much should you increase the CPU speed?

a) Double the CPU speed = E[T] / 2

b) More than double = > (E[T] / 2)

**c)** Less than double

**Rough Argument** : Klingon time is faster than federation time

One Klingon second is half of one federation second

### Question : What is better for minmizing mean response time, many slow servers or one fast server?

Depends on:

- Job size variability
- Job arrival times 

### Question 2 : Job variability is high what are you going to choose?

Answer -> Many small servers

### Question 3 : System load is low. What is better?

Answer -> One large server

**Now assume that the jobs are pre-emptible**
> Have the ability to suspend a job and run it at a later time 
>>>>>>> Stashed changes

### Multi-tenancy 

Providers resources are pooled to serve consumers

Location independence

**Rapid Elasticity** 

- Capabilities can be rapidly and elastically provisioned
- Unlimited resources

**Measured Service** - Service can be measured and charged accordingly

Distributed/Paralell computing - Data storage and computation (Spark)

Virtualization - Decoupling from the physical computing resources (vmware)

### SaaS - Software as a service

Vender provider controlled applications accessed over the network

Dependant on network, bandwidth

Good: Better security than PC

Bad: CSP is in charge of the data

Ugly: User Privacy

### PaaS - Platform as a service

(Google app engine)

Vendor provided development environment

Good

- Rapid Development & deployment
- Scalable & fault-tolerant
- Small start-up cost

Bad

- Inherit all from  SaaS
- Choice of technology is limited by the provider

### IaaS - Infrastructure as a service

Vendor provided and consumer provisioned computing resources

Consumer has complete control over the OS, memory, Storage, Servers and some control over network resources

## Deployment Models

### Private Cloud 

Owned by a private organization

### Community Cloud

Owned by a community

### Public Cloud

Infrastructure is made available to the general public

### Hybrid Cloud

Any combination of the above

### Benefits of CC

- 0 upfront cost
- Just in time Infrastructure
- More efficient resource utilization
- Usage-based costing
- Reduced time to market

### Technical Benefits

- Automation
- Auto-scaling
- Proactive Scaling
- More Efficient Development
- Improved Testability
- Disaster Recovery and Business Continuity
- "Overflow" the traffic to the cloud 

## Scaling 

### Vertical 

Bigger and more powerful hardware

### Horizontal

More Hardware

## Paralell processing

Implement parallellization wherever possible and automate the parallelization

Cloud Computing has been successful due to a combination of Elastic Scaling and Parallel processing

## Probability Review

**Random Experiment** - <OMEGA>

**{All Possible Outcomes}** = `S` = Sample Space

<OMEGA> <ELEMENT_OF> S

### Rolling a die

`S = {1,2,3,4,5,6}`

e.g.

<OMEGA> = 3

**Event** - Subset of sample space. E.g. `a = {1,2,3}` for rolling a die. "First three numbers"

**Probability** - How likely a certain outcome is to occur. Assigned value to outcomes.

- Value must be between 0 and 1

**Support** - Amount of possible events (all possible subsets of the sample space)

**Joint Event** - Intersection of the subsets `P(A and B)`

**Union** - `P(A or B)` - Collection of outcomes that belong to either A or B

`P(A or B) = P(A) + P(B) - P(A and B)`

### Conditional Probability

`P(A | B)` - Probability of `A` given that `B` occurs = `P(A and B) / P(B)`

- If `P(A | B) = 0` it is called a **disjoint** set 
- `P(A and B) = P(A | B) P(B)`
- if `P(A | B) = P(A)`
	- A and B are independent
		- A and B are not necessarily disjoint

If A and B are disjoint -> A and B are not independant 




