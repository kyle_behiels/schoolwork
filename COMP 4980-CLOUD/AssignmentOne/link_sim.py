import random
import numpy as np
import matplotlib.pyplot as plt

DEPARTURE_RATE = 0.8
REPORT_INTERVAL = 1000
SIMULATION_TIME = 100000000
LAMBDA_INTERVALS = 20
_seperator = "========================================================"

def main():
    graph_number = 1
    trials = []
    verify_bernoulli()
    for i in range(1,LAMBDA_INTERVALS):
        mLabel = "graph_"
        mLabel += str(i)
        trials.append(simulate_queue(float(i) / LAMBDA_INTERVALS, True, mLabel))
    plot_all_sims(trials)


#Generate a bernoulli result based on probability prob
def get_bernoulli(prob):
    result = float(random.randint(1,100))/100
    if result <= prob:
        return True
    else:
        return False

#Verify the random generator functionality
def verify_bernoulli():
    print "Verifying bernoulli randomness. Experimental probability should be ~0.7"
    for x in range(0, 5):
        true_vals = 0
        false_vals = 0
    for i in range(0,100000):
        if get_bernoulli(0.7):
            true_vals += 1
        else:
            false_vals += 1
    print "True: ", true_vals, " False: ", false_vals
    print "Experimental probability ", float(true_vals)/float(true_vals+false_vals)
    print _seperator

#Simulate a single queue. Label is what the file name the graph will be saved as
def simulate_queue(arrivalRate, showGraph, label):
    queueLength = 0
    total = 0
    intervals = []
    #check if a packet has arrived
    for i in range(1, SIMULATION_TIME+1):
        if(get_bernoulli(arrivalRate)):
            #got a packet
            queueLength += 1
        #check if a packet has left
        if get_bernoulli(DEPARTURE_RATE) and (queueLength != 0):
            queueLength -= 1
        #Report the current queue length at specified intervals
        if i % REPORT_INTERVAL == 0:
            intervals.append(queueLength)

        total += queueLength

    average = (float(total) / float(SIMULATION_TIME))
    print _seperator
    print "Simulation finished for arrival time ", arrivalRate, "Average waiting time = ", average
    if showGraph:
        show_one_sim_graph(intervals, label)
    return average


#Displays a graph of one simulated queue
def show_one_sim_graph(mList, label):
    t = np.arange(0.0, float(SIMULATION_TIME/REPORT_INTERVAL), 1)
    plt.plot(t, mList)
    plt.xlabel("Time Interval (x1000)")
    plt.ylabel("Queue Length")
    plt.title("Queue Length vs Time Interval in single server queues")
    plt.grid(True)
    savefigname = ""
    savefigname += label
    savefigname += ".png"
    print savefigname
    plt.savefig(savefigname)
    plt.clf()
#Shows the trend of all simulated graphs as arrival rate increases
def plot_all_sims(mList):
    x = []
    for i in range(1, len(mList)+1):
        x.append(i * 0.05)

    plt.axis([0.05, len(mList)*0.05, 0, 5])
    plt.plot(x, mList)
    plt.xticks(np.arange(0, len(mList)*0.05, 0.05))
    plt.xlabel("Arrival Rate")
    plt.ylabel("Average Waiting Time")
    plt.title("Average Waiting Time by Arrival Time")
    plt.grid(True)
    plt.show()
    plt.clf()

main()