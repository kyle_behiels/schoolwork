from pulp import *

prob = LpProblem("Virtual Machine Placement", LpMinimize)

serve_one = LpVariable("Server One", 0, 100, LpInteger)
serve_two = LpVariable("Server Two", 0, 100, LpInteger)
serve_three = LpVariable("Server Three", 0, 100, LpInteger)
serve_four = LpVariable("Server four", 0, 100, LpInteger)
serve_five = LpVariable("Server five", 0, 100, LpInteger)
serve_six = LpVariable("Server Six", 0, 100, LpInteger)
serve_seven = LpVariable("Server Seven", 0, 100, LpInteger)

prob += serve_one + serve_two + serve_three + serve_four + serve_five + serve_six + serve_seven , "Total server count"
prob += serve_three + serve_four + (2 * serve_five) >= 20
prob += serve_two + serve_four + serve_six + (2 * serve_seven) >= 90
prob += serve_one + serve_two >= 15

prob.writeLP("LinearModel.lp")

prob.solve()

print("Status:", LpStatus[prob.status])

for v in prob.variables():
    print(v.name, "=", v.varValue)

print("min bound on servers is ", value(prob.objective))


