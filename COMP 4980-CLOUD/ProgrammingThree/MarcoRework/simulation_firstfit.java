import java.util.*;

class SimulationFirstFit {

    static int[] NS = {10, 100, 1000};
    static double LAMBDA = 0.9;
    static int RUNTIME = 1000000;
    static Random rand = new Random();

    public static void main(String[] args) {
        ArrayList<Server> serverQueue = new ArrayList<>();

        // Do departures here

        // First fit
        for (int N : NS) {
            System.out.println("N = " + N);
            final long startTime = System.nanoTime();

            int averageServers = 0;

            for (int r = 0; r < RUNTIME; r++) {
                // System.out.println(r + "/" + RUNTIME);

                // Departures
                int serverQueueLength = serverQueue.size();

                for (int i = serverQueueLength - 1; i >= 0; i--) {

                    Server nServe = serverQueue.get(i);
                    // System.out.println("Starting server size" + nServe.vms.size());
                    int vmsLength = nServe.vms.size();
                    for (int x = vmsLength - 1; x >= 0; x--) {
                        if (nServe.vms.get(x).willDepart()) {            // Departure occurs
                            nServe.remainingCapacity += nServe.vms.get(x).cpuUsage;
                            nServe.vms.remove(x);
                        }
                    }
                    nServe.vms.trimToSize();
                    // System.out.println("End server size" + nServe.vms.size());

                    if (nServe.vms.size() == 0) {
                        serverQueue.remove(i);
                        // System.out.println("REMOVING SERVER");
                    }

                }
                serverQueue.trimToSize();
                // System.out.println("ServerQueue length = " + serverQueue.size());
                // Arrivals
                int arrivals = binomialDist(N, LAMBDA);
                // System.out.println("Processing " + arrivals + " arrivals.");
                for (int i = 0; i < arrivals; i++) {
                    int vmType = rand.nextInt(3) + 1;
                    boolean foundServer = false;
                    VirtMachine virtMachine = new VirtMachine(
                            vmType == 1 ? 4 : vmType == 2 ? 2 : 1,
                            vmType == 1 ? 10 : vmType == 2 ? 8 : 30
                    );
                    for (Server server : serverQueue) {
                        if (server.remainingCapacity >= virtMachine.cpuUsage) {
                            server.vms.add(virtMachine);
                            server.remainingCapacity -= virtMachine.cpuUsage;
                            foundServer = true;
                            break;
                        }
                    }
                    if (!foundServer) {
                        Server nServer = new Server();
                        nServer.vms.add(virtMachine);
                        nServer.remainingCapacity -= virtMachine.cpuUsage;
                        serverQueue.add(nServer);
                    }
                }

                averageServers += serverQueue.size();
            }
            System.out.println("----------------------------------------------------------");
            System.out.println("Average Servers = " + (averageServers / (float) RUNTIME));
            final long endTime = System.nanoTime();
            System.out.println("Finished Execution in " + ((endTime - startTime) / 1000000000.0) + "Seconds");
            System.out.println("==========================================================");
        }
    }

    public static int binomialDist(int trials, double probability) {
        int successes = 0;

        for (int i = 0; i < trials; i++) {
            if (rand.nextDouble() < probability) {
                successes++;
            }
        }
        return successes;
    }

    public static class VirtMachine {
        int meanTtl;
        int cpuUsage;

        VirtMachine(int ttl, int cpu) {
            this.meanTtl = ttl;
            this.cpuUsage = cpu;
        }

        public Boolean willDepart() {
            return rand.nextDouble() <= 1.0 / meanTtl;
        }
    }

    public static class Server {
        ArrayList<VirtMachine> vms = new ArrayList<>();
        public int remainingCapacity = 8;

        Server() {
        }

    }
}
