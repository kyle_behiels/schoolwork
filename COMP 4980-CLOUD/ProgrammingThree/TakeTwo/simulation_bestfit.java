import java.util.LinkedList; 
import java.util.*;

class simulation{

    static int[] NS = {10, 100, 1000};
    static double LAMBDA = 0.9;
    static int RUNTIME = 1000;
    static Random rand = new Random();

    public static void main(String[] args) {
        ArrayList<Server> serverQueue = new ArrayList<>();

        // Do departures here

        // Best fit
        for(int N : NS){
            System.out.println("N = " + N);
            final long startTime = System.nanoTime(); 
            serverQueue = new ArrayList<>();
            int averageServers = 0;

            for(int r = 0; r < RUNTIME; r++){
                // System.out.println(r + "/" + RUNTIME);

                // Departures
                int serverQueueLength = serverQueue.size();

                for(int i = serverQueueLength-1; i >= 0; i --){

                    Server nServe = serverQueue.get(i);
                    // System.out.println("Starting server size" + nServe.vms.size());
                    int vmsLength = nServe.vms.size();
                    for(int x = vmsLength-1; x >= 0; x--){
                        if(nServe.vms.get(x).willDepart()){            // Departure occurs
                            nServe.load -= (nServe.vms.get(x).cpuUsage);
                            nServe.vms.remove(x);
                        }
                    }
                    nServe.vms.trimToSize();
                    // System.out.println("End server size" + nServe.vms.size());

                    if(nServe.vms.size() == 0){
                        serverQueue.remove(i);
                        // System.out.println("REMOVING SERVER");
                    }

                }
                serverQueue.trimToSize();
                // Arrivals
                int arrivals = binomialDist(N, LAMBDA);
                // System.out.println("Processing " + arrivals + " arrivals.");
                int[] serverCpuUsage = {4, 3, 1};
                for(int i = 0; i < arrivals; i++){
                    int vmType = rand.nextInt(3)+1;
                    Boolean foundServer = false;
                    int currentBest = 8;
                    int currentBestIndex = 0;
                    
                    switch (vmType) {
                        case 1:                                                 //VM Type 1
                            for(Server server : serverQueue){
                                if(server.freeCpu() == serverCpuUsage[0]){
                                    currentBestIndex = serverQueue.indexOf(server);
                                    foundServer = true;
                                    break;
                                }
                                else if(server.freeCpu() > serverCpuUsage[0] && (currentBest > server.freeCpu())){
                                    currentBest = server.load;
                                    foundServer = true;
                                    currentBestIndex = serverQueue.indexOf(server);
                                }
                            }
                            if(foundServer){
                                Server nServer = serverQueue.get(currentBestIndex);
                                nServer.vms.add(new VirtMachine(10, serverCpuUsage[0]));
                                nServer.load += serverCpuUsage[0];
                                serverQueue.add(currentBestIndex, nServer);
                                serverQueue.remove(currentBestIndex+1);


                            }
                            if(!foundServer){
                                Server nServer = new Server();
                                nServer.vms.add(new VirtMachine(10, serverCpuUsage[0]));
                                nServer.load+=serverCpuUsage[0];
                                serverQueue.add(nServer);
                            }
                            break;
                        case 2:                                                 //VM Type 2
                            for(Server server : serverQueue){
                                if(server.freeCpu() == serverCpuUsage[1]){
                                    currentBestIndex = serverQueue.indexOf(server);
                                    foundServer = true;
                                    break;
                                }
                                else if(server.freeCpu() > serverCpuUsage[1] && (currentBest > server.freeCpu())){
                                    currentBest = server.load;
                                    foundServer = true;
                                    currentBestIndex = serverQueue.indexOf(server);
                                }
                            }
                            if(foundServer){
                                Server nServer = serverQueue.get(currentBestIndex);
                                nServer.vms.add(new VirtMachine(8, serverCpuUsage[1]));
                                nServer.load+=2;
                                serverQueue.add(currentBestIndex, nServer);
                                serverQueue.remove(currentBestIndex+1);


                            }
                            if(!foundServer){
                                Server nServer = new Server();
                                nServer.vms.add(new VirtMachine(8, serverCpuUsage[1]));
                                nServer.load +=  serverCpuUsage[1];
                                serverQueue.add(nServer);
                            }
                            break;
                        case 3:                                                 //VM Type 3
                            for(Server server : serverQueue){
                                if(server.freeCpu() == serverCpuUsage[2]){
                                    currentBestIndex = serverQueue.indexOf(server);
                                    foundServer = true;
                                    break;
                                }
                                else if(server.freeCpu() > serverCpuUsage[2] && (currentBest > server.freeCpu())){
                                    currentBest = server.load;
                                    foundServer = true;
                                    currentBestIndex = serverQueue.indexOf(server);
                                }
                            }
                            if(foundServer){
                                Server nServer = serverQueue.get(currentBestIndex);
                                nServer.vms.add(new VirtMachine(30, serverCpuUsage[2]));
                                nServer.load += serverCpuUsage[2];
                                serverQueue.add(currentBestIndex, nServer);
                                serverQueue.remove(currentBestIndex+1);

                            }
                            if(!foundServer){
                                Server nServer = new Server();
                                nServer.vms.add(new VirtMachine(30, serverCpuUsage[2]));
                                nServer.load += serverCpuUsage[2];
                                serverQueue.add(nServer);
                            }
                            break;
                    }
                }

                if(r == 0){
                    averageServers += serverQueue.size();
                }
                else{
                    averageServers = (averageServers + serverQueue.size()) / 2;
                }
            
            }

            System.out.println("----------------------------------------------------------");
            System.out.println("Average Servers = " + averageServers);
            final long endTime = System.nanoTime();
            System.out.println("Finished Execution in " + ((endTime-startTime) / 1000000000.0) + "Seconds");
            System.out.println("==========================================================");


        }
        
        
    }

    public static int binomialDist(int trials, double probability){
        int successes = 0;
        
        for(int i = 0; i < trials; i++){
            if(probability > rand.nextDouble()){
                successes++;
            }
        }
        return successes;
    }

    public static int getServerType(){
        return rand.nextInt(3);
    }


    public static class VirtMachine{
        int meanTtl;
        int cpuUsage;
            
        VirtMachine(int ttl, int cpu){
            this.meanTtl = ttl;
            this.cpuUsage = cpu;
        }

        public Boolean willDepart(){
            if(rand.nextInt(meanTtl)+1 == 1) return true;
            return false;
        }
    }

    public static class Server {
        ArrayList<VirtMachine> vms = new ArrayList<>();
        public int load = 0;
        Server(){

        }
        public int freeCpu(){
            return 8 - load;
        }

    }

}



