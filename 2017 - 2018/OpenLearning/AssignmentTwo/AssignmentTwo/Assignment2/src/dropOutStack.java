
public class dropOutStack<T> {
	
	public final int maxSize;
	public int currentSize;
	
	//Custom exceptions for when the user is dumb...
	Exception emptyStackException = new Exception("The stack is empty!");
	Exception stackSizeException = new Exception("The stack size must be greater than 0.");
	
	public dropOutStack(int stackSize) throws Exception{
		first = new Node(null);
		last = new Node(null);
		
		//Define the first and last node for popping and dropping
		if(stackSize>=1){
			maxSize = stackSize;
		}
		else{
			throw stackSizeException;
		}
	}
	//Basic getter for the size of the stack
	public int size(){
		return maxSize;
	}

	//Definition of a node or "Link"
	private class Node{
		private T data;
		private Node next;
		private Node previous;
		public Node(T data){
			this.data = data; 
		}
	}
	
	//Define first and last nodes
	private Node first;
	private Node last;
	
	//Check if the stack is empty
	public boolean isEmpty(){
		return first == null;
	}
	
	//Get the top element of the stack without popping it
	public T peek(){
		if(last!=null){
			return last.data;
		}
		else{
			return null;
		}
	}
	public void push(T newData){
		
		Node node = new Node(newData);
		if(first == null){
			first = node;
		}
		if(first.data != null){		//If the stack already contains elements...
			if(currentSize<=maxSize){		//If the stack is not yet full...
				
				currentSize++;		// Add one to the size of the stack
			
				//Migrate data from the head of the stack to a new, arbitrary node
				node.data = first.data;
				node.next = first.next;
				node.previous = first;
				
				//Assign new data to the new head of the stack and link the newly created node
				first.data = newData;
				first.next = node;
			}
			else{			// If the stack is full...
				/*
				 * Overwrite the last element using the element contained in the last.previous
				 * variable, thus "dropping out" the bottom of the stack.
				 */
				System.out.println("The stack is full");
				first = first.next;
			}
		}
		else{		// If this is the first push command
			first.data = newData;
			last.data = newData;
			last.previous = first;
		}
		
	}
	
	//Returns and removes the top value of the stack
	public T pop() throws Exception{
		if(first != null){
			T returnData = first.data;
			first = first.next;

			currentSize--;
			return returnData;
		}
		else{
			throw emptyStackException;
		}
	}
	
	
}
