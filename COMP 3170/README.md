# COMP 3170 - Applied Artificial Intelligence

**Professor** - Manhoon Lee

| Topics | 
---
| Classical Algorithms | 
| Fuzz |
| Learning vs. Programming |
| Intelligence through Evolution |
| Collective Intelligence |
| Emergence and Artificial Life |

# Notes

## Deterministic vs. Non-deterministic

**Deterministic** - The same input will always yield the same output

**Non-Deterministic** - The same inputs have the potential to yield different outputs

### How do we know that a system is intelligent?

**Chinese Room Experiment** - If a question is asked in chinese and a black box responds in fluent chinese, what is in the box?

## Knowledge Representation

> If, for a given problem we have a means of checking a proposed solution, then we can solve the problem by testing all possible answers 

\- Marvin Minsky

Computers need a **formal representation of a problem** in order to solve it

### Search spaces

Representation is *Problem specific*

Many problems in AI can be represented as search spaces

A **Search Space** is a representation of the set of all possible choices in a given problem, one or more of which are the solution or the goal to the problem

Are search spaces always limited?

## Search methodologies

