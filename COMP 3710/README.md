# Applied Artificial Intelligence

**Instructor** : Manhoon Lee
**Office** : HL 424
**Phone** : (250)-377-6022

Office Hours

10:30 - 12:00 Mon - Wed

## Evaluation

Assignments - 20%
Project - 30%
Two midterms - 50%
Final Exam - None

Minimum of 80% attendance

---

# Notes

## Trees

- **Trees** - Are easier to handle than graphs
- Root Node has no predecesor
- Leaf Node has no successor
- Goal nodes - What we are tryin to achieve

**Breadth First Search** - Visit every level first starting with root (Implement w/ Queue)

**Depth First Search** - Explore from root all the way to child, visiting each child one at a time (Implement w/ stack)

### Missionaries and Cannibals

- Three missionaries and three cannibals want to cross a river
- Canoe can hold up to two people
- Can never be more cannibals than missionaries on either side of the river
- Aim:  Get all sagely across the river without any missionaries being had

### Djikstra's shortest path algorithm

- Distance from the start node is used instead
- Only looks at past information

### Heuristics

- A rule or other piece of information that is used to make methods, such as search, more efficient or effective
- In search, often use a heuristic evaluation function

