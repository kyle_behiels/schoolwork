// ALGORITHM ONE

function computeTourLength(tour, cities)
{
    var sum = 0;
    
    for (i = 0; i < tour.length; i++) {
        if (i < tour.length-1) // not the last one
            sum += Math.sqrt(Math.pow(cities[tour[i]].x - cities[tour[i+1]].x, 2)
                                + Math.pow(cities[tour[i]].y - cities[tour[i+1]].y, 2));
        else
            sum += Math.sqrt(Math.pow(cities[tour[i]].x - cities[tour[0]].x, 2)
                                + Math.pow(cities[tour[i]].y - cities[tour[0]].y, 2));
    }
    
    return sum;
}

var cities = []; 
cities[0] = {}; cities[0].x = 0; cities[0].y = 50;
cities[1] = {}; cities[1].x = 10; cities[1].y = 35;
cities[2] = {}; cities[2].x = 40; cities[2].y = 15;
cities[3] = {}; cities[3].x = 30; cities[3].y = 20;
var tour = [];
tour[0] = 1; tour[1] = 0, tour[2] = 3; tour[3] = 2;  // tour: [1, 0, 3, 2]

var ttd = computeTourLength(tour, cities);

//ALGORITHM TWO

<div id='tr2-output'></div>

<script>
function evaluateFitnesses(tour_lengths, cities)
{
    var x = 0;
    for (var i = 0; i < tour_lengths.length; i++)
        x += 1/tour_lengths[i];  // the sum of the reverse of tour lengths
    var fitnesses = [];
    for (var i = 0; i < tour_lengths.length; i++)
        fitnesses[i] = (1/tour_lengths[i]) / x;  // [0, 1); total 1
    
    return fitnesses;
}

var tours = [];
tours[0] = []; tours[0][0] = 1; tours[0][1] = 0, tours[0][2] = 3; tours[0][3] = 2;  // tour0: [1, 0, 3, 2]
tours[1] = []; tours[1][0] = 0; tours[1][1] = 2, tours[1][2] = 1; tours[1][3] = 3;  // tour1: [0, 2, 1, 3]
tours[2] = []; tours[2][0] = 1; tours[2][1] = 3, tours[2][2] = 2; tours[2][3] = 0;  // tour2: [1, 3, 2, 0]
tours[3] = []; tours[3][0] = 3; tours[3][1] = 0, tours[3][2] = 1; tours[3][3] = 2;  // tour3: [3, 0, 1, 2]
var ttds = [];
ttds[0] = computeTourLength(tours[0], cities);
ttds[1] = computeTourLength(tours[1], cities);
ttds[2] = computeTourLength(tours[2], cities);
ttds[3] = computeTourLength(tours[3], cities);

var fitnesses = evaluateFitnesses(ttds, cities);

$('#tr2-output').html(fitnesses[0] + ", " + fitnesses[1] + ", " + fitnesses[2] + ", " + fitnesses[3]);
</script>
                    

// Algorithm Three

<div id='tr3-output'></div>

<script>
function makeRouletteWheel(fitnesses)
{
    var wheel = [];
    for (var i = 0; i < fitnesses.length; i++)
        if (i == 0)
            wheel[0] = fitnesses[0];
        else if (i < fitnesses.length - 1)
            wheel[i] = wheel[i-1] + fitnesses[i];  // accumulation of fitnesses
        else
            wheel[i] = 1;
    
    return wheel;
}

function selecTour(wheel)
{
    var r;
    r = Math.random();  // [0, 1)
    for (var i = 0; i < wheel.length; i++){
        if (r <= wheel[i])
            return i;
    }
    return selecTour(wheel);
}

var wheel = makeRouletteWheel(fitnesses);

var selected_tour_no_0, selected_tour_no_1;
selected_tour_no_0 = selecTour(wheel);
selected_tour_no_1 = selecTour(wheel);

$('#tr3-output').html(selected_tour_no_0 + ", " + selected_tour_no_1);
</script>
                    

// Algorithm Four

<div id='tr4-output'></div>

<script>
function applyCrossover(parents)  // Idea 1; just middle point
{
    var offsprings = [];
    offsprings[0] = []; offsprings[1] = [];
    var cross_point = Math.floor(parents[0].length / 2);  // should not be 0
    
    for (var i = 0; i < cross_point; i++)
        offsprings[0][i] = parents[0][i];
    for (var i = cross_point; i < parents[0].length; i++)
        offsprings[0][i] = findNotUsedCity(offsprings[0], i, parents[1]);

    for (var i = 0; i < cross_point; i++)
        offsprings[1][i] = parents[1][i];
    for (var i = cross_point; i < parents[1].length; i++)
        offsprings[1][i] = findNotUsedCity(offsprings[0], i, parents[0]);
    
    return offsprings;
}

function findNotUsedCity(offspring, x, parent)  // Note that offspring[0] ... offspring[x-1] are already decided.
{
    var i, j, k;
    for (i = x; i < parent.length; i++)  // check from x to the end
        if (!isCityUsed(offspring, x, parent[i]))
            return parent[i];
    for (i = 0; i < x; i++)  // check from 0 to x-1
        if (!isCityUsed(offspring, x, parent[i]))
            return parent[i];
}

function isCityUsed(offspring, x, city)  // Note that offspring[0] ... offspring[x-1] are already decided.
{
    for (var i = 0; i < x; i++)
        if (offspring[i] == city)
            return true;
    
    return false;
}

var parents = []; parents[0] = tours[selected_tour_no_0]; parents[1] = tours[selected_tour_no_1];
var offsprings = [];
offsprings = applyCrossover(parents);

$('#tr4-output').html(offsprings[0].toString() + ", " + offsprings[1].toString());
</script>
                    

// Algorithm Five


<div id='tr5-output'></div>

<script>
function applyMutation(offspring)  // Idea 1
{
    var city_no_0, city_no_1;
    
    // select two different cities
    
    city_no_0 = Math.floor(Math.random() * offspring.length);
    while(true) {
        city_no_1 = Math.floor(Math.random() * offspring.length);
        if (city_no_0 != city_no_1)
            break;
    }
    
    // make sure city numbers in order
    
    if (city_no_0 > city_no_1) {
        var tmp = city_no_0;
        city_no_0 = city_no_1;
        city_no_1 = tmp;
    }
    
    // apply mutation
    
    for (var i = city_no_1; i > city_no_0; i--) {  // swap with the one index previous one
        var tmp = offspring[i];
        offspring[i] = offspring[i-1];  
        offspring[i-1] = tmp;
    }
}

var RATE_MUTATION = 0.05;

if (Math.random() < RATE_MUTATION)
    applyMutation(offsprings[0]);
if (Math.random() < RATE_MUTATION)
    applyMutation(offsprings[1]);

$('#tr5-output').html(offsprings[0].toString() + ", " + offsprings[1].toString());
</script>
                    
         
