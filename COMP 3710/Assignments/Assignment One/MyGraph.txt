// number of nodes
8
// node id and its content (strings)
0   Q
1   W
2   E
3   R
4   T
5   Y
6   U
7   I
// Adjacency matrix
-1   3   4  -1  -1  -1  -1  -1
 3  -1  -1   7  -1  -1  -1  -1
 4  -1  -1  -1  12  -1  -1  -1
-1   7  -1  -1  -1  -1   4  -1
-1  -1  12  -1  -1   9   7  -1
-1  -1  -1  -1   9  -1  -1  -1
-1  -1  -1   4   7  -1  -1   3
-1  -1  -1  -1  -1  -1   3  -1
