# Human Computer Interaction

**Instructor** : Piper Jackson

Moodle : yellow noun

## Evaluation

- Attendance 5%
- Seminar Exercises 5%
- Quizzes 10%
- Team Project 25% - Groups of ~4
- Midterm Exam 20%
- Final Exam 35%

Minimum 50% on both exam and non-exam portions. 90% mandatory attendance.

## Team Project

**7 Phases**

- Week 2-3
	- Pitch(Conceptual Model)

3 Team members is best, 4 is okay

Programming and user interace should be part of the project

Using mobile platforms (Android or IOS) and data science methods is encouraged

Can be based on work for/from other classes

Must be a useful and practical application

- Increase accessibility
- Enabling some activity
- Improving effectiveness or efficiency

Should be testable with target users

Arrange a time to visit my office (HL 408)

Best time is Wednesday 9-11

**One Pager**
- Title
- Members
- Verbal Description

## Introduction

### Usability goals

- Effectiveness
    - Refers to how good a product is at doing what it is supposed to do
- Efficient
    - The way a product supports users in carrying out their tasks
- Safety
    - Protecting the users from dangerous conditions and undesirable situations
- Utility
    - Does it have all of the functionality that you want/need
- Learnability
    - How easy is it learn
- Memorability
    - How easy is it to remember how to learn

### User Experience Goals - How does it feel to use that system

Are there trade-offs between the two kinds of goals?

## Design

**Visibility** - Functions are visible showing what to do next


**Feedback** - Send information back to they know what is going on

**Constraints** - Designs that prevent users from doing something wrong

**Logical or ambiguous design**

**Consistency** - Design interfaces to have similar operations and use similar elements for similar tasks. The main benefit is ease of learning.

**Internal vs External Consistency**
- Internal consistency refers to design within the same program
- External consistency refers to all design as a whole

**Affordance: to give a clue** - Refers to an attribute of an object that allows people to know how to use it

# Chapter 2 - Understanding and Conceptualizing Interaction

**Problem space**

- What do you want to create?
- What are your assumptions?
- Will it achieve what you hope it will?

**What is an assumption?**

- Taking something for granted when it requires further investigation.


**What is a claim?**

- Stating something to be true when it is still open to question.


### Framework for analysing the problem space

- Are there problems with an existing product or experience? If so what are they?
- Why do you think there are problems?
- How do you think your proposed design ideas might overcome these?
- If you are designing for a new user experience how do you think your proposed design ideas support change or extend current ways of doing things?

### Activity

- What are the assumptions and claims made about 3D TV?
- Are they realistic or a wish list?
    - People would not mind wearing the glasses that are needed
    - People would really enjoy the enhanced experience

### From problem space to design space

**Conceptual Model** -> A high level description of how a system is organized and operates

**Enables** -> Designers to straighten out their thinking before they start laying out their widgets

## Paradigm

- Inspiration for a Conceptual model
- General approach adopted by a community for carrying out research

### Examples of new paradigms

- Ubiquitous computing
- Pervasive computing
- Wearable computing
- Tangible bits, AR
- Attentive environments
- Transparent Computing

### Visions

- Driving force that frames research and development
- Invites people to imagine what life will be like in 10, 15 or 20 years time
- Provide concrete scenarios of how society can use the next generation of imagined tech
- Also raise many questions concerning privacy and trust

### Theory

- Explanation of a phenomenon
	- Information processing that explains how the mind, or some aspect of it, is assumed to work
- Can help identify factors
	- Cognitive, social, and affective, relevant to the design and evaluation of interactive products

### Models
> "Useful Simplification"

- Simplification of an HCI phenomenon
	- Intended to make it easier for designers to predict and evaluate alternative Designs
	- Abstracted from a theory coming from a contributing discipline like psychology or keystroke model

### Framework

- Set of interrelated concepts and/or specific questions fo 'what to look for'
- Many in interaction design
- Provide advice on how to design

# Establishing Requirements - Chapter 10

## Mapping: Good or Bad?

### Four different interaction types

1. Instructional
2. Conversational
3. Manipulation
4. Exploration

### What, how and why?

**What needs to be achieved?**

1. Understand as much as possible about the users task context
2. Produce a stable set of requirements

**How can this be done?**

- Data gathering activities
- Data analysis activities
- Expressiono as 'requirements'

Getting requirements is one of the most common points of failure

### Establishing Requirements

- What do they want vs what do they need
- Why 'establish'
	- Requirements arise from understanding users needs.
	- Requirements can be justified

**Volere Shell** - Industry standard form for requirement specification

**Requirements** -
"A statement about an intended product that specifies what it should do or how it should perform"

### Environment of context of use

- Physical
- Social
- Orginizational
