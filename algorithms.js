
// Algorithm One

var rules = []; 
rules['Frog'] = [['Croak', 'EatFly']];
rules['Canary'] = [['Chirp', 'Sing']];
rules['Green'] = [['Frog']];
rules['Yellow'] = [['Canary']];
var facts = ['Croak', 'EatFly'];
var colors = ['Yellow', 'Green'];

var str = rules['Frog'] + '; ' + rules['Canary'] + '; ' + rules['Green'] + '; ' + rules['Yellow'];


// Algorithm Two

function isFact(facts, ps)
{
    if (facts.indexOf(ps) >= 0)  // facts is a linear array. You can check the index of ps.
        return true;
    else
        return false;
}

var str = isFact(facts, 'Yellow');

// Algorithm Three

function thereAreRules(rules, ps)
{
    if (rules[ps] != undefined)  // rules is an associative array. You can check if the value is undefined.
        return true;
    else
        return false;
}

var str = thereAreRules(rules, 'Frog');

// Algorithm Four

function BC(rules, facts, ps)
{
    // if ps is a fact
    if(isFact(facts, ps)){
        return true;
    }
        
    if (!thereAreRules(rules, ps))  // if there is no rule used for ps;
        return false; //Might be just ps

    // for multiple rules
    
    var valid = false;
    for (var i = 0; i < rules[ps].length; i++)  // the backward chaining results for rules should be ORed. 
    {
        // for multiple propositions in a rule
        // the backward chaining results for positional symbols, rules[ps][i][], in a rule should be ANDed.
        let innerValid = true;
        for(var x = 0; x < rules[ps][i].length; x ++){
             let bullshit = BC(rules, facts, rules[ps][i][x]);
             innerValid = innerValid && bullshit;

        }
        if(innerValid){
            valid = true;
            break;
        }

    }

    if (valid)
        facts.push(ps);  // push it into the facts queue

    return valid;
}
                   
          
                    
                    