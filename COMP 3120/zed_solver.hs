-- import Data.Matrix
import Data.List
import Control.Parallel

--- Sanity functions, mtrx = Matrix | indx = Index

fixUglyInput arr = [if(n /= 0 && n /= 1)then reverse (arr!!n) else arr!!n | n <- [0..3]]

printMap :: (Foldable t, Show a) => t a -> IO ()
printMap mtrx = do
    print "==Solution=="
    (mapM_ print mtrx);
    print "============"

getCol :: [[a]] -> Int -> [a]
getCol mtrx indx = [x!!indx | x <- mtrx];

walkArr :: [Int] -> Int -> Int -> Int -> Int
walkArr arr n ind bestPost | ind == (n) = 1
walkArr arr n ind bestPost = do
    if (arr!!ind) > bestPost then
        (walkArr arr n (ind+1) (arr!!ind)) + 1
    else
        (walkArr arr n (ind+1) bestPost)

--- Clean wrapper for walkArr
walkArrW :: [Int] -> Int -> Int
walkArrW arr n = walkArr arr n 0 (arr!!0)
    
---Given the two values on either side of a column or row as a list, get all legal
---values for that list (b = bottom, a = top, n = num vals)
getLegalLists :: Int -> Int -> Int -> [[Int]]
getLegalLists a b n = [x | x <- permutations [1..n], (walkArrW x n) == a, ((walkArrW (reverse x) n) == b)]

checkList :: Int -> Int -> [Int] -> Int -> Bool
checkList a b arr n = if (walkArrW arr n == a) && (walkArrW (reverse arr) n == b) then True else False

verifySumCols :: [[Int]] -> Int -> Bool
verifySumCols mat n = do
    let goodCols = [ getCol mat x | x <- [0..n-1], sum (getCol mat x) == sum [1..n]]
    ((length goodCols) == n)

-- Where mat = the matrix and arr = the values on the outside
checkMatrixCols :: [[Int]] -> [[Int]] ->  Int -> Bool
checkMatrixCols mat arr n = do
    let validRet = map (True &&) [checkList (fst (zfc arr x)) (snd (zfc arr x)) (getCol mat x) n |  x <- [0..n-1]]
    not (elem False validRet)

-- Zeds for column
zfc :: [[t]] -> Int -> (t, t)
zfc arr n = ((arr!!0)!!n, (arr!!2)!!n)

-- Zeds for row
zfr :: [[t]] -> Int -> (t, t)
zfr arr n = ((arr!!3)!!n, (arr!!1)!!n)

-- Get filtered matrices for all columns
-- Where n is size of board (4x4 n=4)
listsForRows :: [[Int]] -> Int -> [[[Int]]]
listsForRows arr n = [getLegalLists (fst (zfr arr x)) (snd (zfr arr x)) (n) | x <- [0..n-1]]
 
combos :: [[a]] -> [[a]]
combos [] = [[]]
combos ([]:ls) = combos ls
combos ((h:t):ls) = map (h:) (combos ls) ++ combos (t:ls)
-- From mail.haskell.org

-- Get legal matrices (Before checking the legality of cols)
solve :: [[Int]] -> Int -> [[[Int]]]
solve arr n = [x | x <- combos (listsForRows arr n), length x == n, checkMatrixCols x arr n, verifySumCols x n]

zed :: [[Int]] -> IO ()
zed arr =  mapM_ printMap (solve (fixUglyInput arr) (length (arr!!0)));

{-

solve was the function I wrote origionally because the way the inputs in the question were formatted did not make sense to me. zed simply
reformats the inputs and calls solve so I guess it is just a wrapper for solve.

-}
