{-# LANGUAGE ParallelListComp #-}
import System.IO
import Control.Monad
import Data.List.Split
import Data.Char
import Data.Map (fromListWith, toList)

--- Hopefully ParallelListComp is legal
myZipWith :: (a1 -> b -> a2) -> [a1] -> [b] -> [a2]
myZipWith a b c = [uncurry (a) (x, y) | x <- b | y <- c]

---This feels like cheating
myHead :: [b] -> b
myHead n = fst (n!!0, n!!1)

countWords :: IO ()
countWords = do
    putStrLn "Please enter a file name"
    fileName <- getLine
    handle <- openFile fileName ReadMode
    contents <- hGetContents handle
    let mWords = (words contents)
    print (toList (fromListWith (+) [(x, 1) | x <- mWords]))
    

    