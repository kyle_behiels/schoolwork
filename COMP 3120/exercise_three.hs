import Data.List

triSeries :: Floating a => Int -> [a]
triSeries n = take n (iterate (\x -> x + (((sqrt (1+8*x)-1)/2)+1))1)

triSquares :: (RealFrac a, Floating a) => Int -> [a]
triSquares x = filter (\x -> fromIntegral(floor (sqrt x)) == sqrt x) (triSeries x)


addSpace :: Char -> [Char]
addSpace a = a:[] ++ " "

addSpaces :: [Char] -> [[Char]]
addSpaces xs = map (addSpace) xs

spreadOut :: [Char] -> [Char]
spreadOut xs = intercalate " " (addSpaces xs)

sumProd :: [Integer] -> (Integer, Integer)
sumProd lst = (sum lst, product lst)

