lgCand :: Integral a => a -> a
lgCand = floor.sqrt.fromIntegral

isPrime x = not (any (==0) [x `mod` n | n <- [2..lgCand x]])
primes n = [ x | x <- [2..n], isPrime x]

-- C(n,k) = C(n-1,k-1) + C(n-1,k) for 0 < k < n.


choose n k 
    | k == 1 = n
    | k == n = 1
    | k == 0 = 1
    | otherwise = ((choose (n-1) (k-1)) + (choose (n-1) k))


-- pascal x = [choose z y | z <- [0..x], y <- [0..z]]

pascal x = [[choose z y | y <- [0..z]] | z <- [0..x]]


{-
Hey Professor,

As you know this function gave me some trouble.I understand 
how it works and I know I am close as the verticalbar denotes
where it would normally return a String to a list. Just 
can't quite figure out the syntax in the context of this
question.

Thanks,

Kyle
-}
tokenize :: Char -> String -> [Char] 
tokenize _ [] = []
tokenize n xs = tok n xs []
    where
    tok n xs acc
        | xs == [] = acc
        | n == head xs = (acc ++ "|") ++ tokenize n (tail xs)
        | otherwise = tok n (tail xs) (acc ++ [head xs])