% Question One
% ========================
% ?- likes(phil,injera).
% true.

% ?- likes(hamed,X).
% X = uni.

% ?- likes(piper,X).
% X = gejang ;
% X = maatjes ;
% X = natto.

% ?- likes(jetset,X).
% false.

% ?- likes(X,durian).
% false.

% ?- likes(X,garlic).
% X = eri ;
% X = phil.


difficultfood(bluecheese).
difficultfood(durian).
difficultfood(garlic).
difficultfood(gejang).
difficultfood(injera).
difficultfood(jalapeno).
difficultfood(maatjes).
difficultfood(natto).
difficultfood(slurpee).
difficultfood(stinkytofu).
difficultfood(uni).
difficultfood(marshmallow).
difficultfood(peanuts).
difficultfood(olives).
difficultfood(rocks).

likes(everybodyButMe, peanuts).
likes(italians, olives).
likes(nobody, rocks).
likes(eri,garlic).
likes(eri,jalapeno).
likes(eri,slurpee).
likes(hamed,uni).
likes(phil,bluecheese).
likes(phil,garlic).
likes(phil,injera).
likes(piper,gejang).
likes(piper,maatjes).
likes(piper,natto).
likes(meredith,marshmallow).

jetsetLikes(B) :- difficultfood(B). 