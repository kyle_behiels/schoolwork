# Programming Languages

Instructor : Piper Jackson

Office Hours - 9:00 -> 11:00 Wednesday

moodle: entirely simplest

# Evaluation

Programming in Haskell and Prolog

- Attendance - 5%
- Excercises - 5%
- Assignments - 20%
- Midterm - 30%
- Final - 40%
- 50% on exam and 50% on non exam
- 90% of scheduled attendance

# Supplement

Programming Languages: Application and Interpretation

---

**Programming Language** - A formal language for creating programs for a computer 

- Formal Language: Set of symbols and rules
- Program: Set of instructions for computation
- Computation: Calculation that follows a well-defined model
- Algorithm: A clear specification of how to solve a problem

# Models of Computation

**Finite State Machines** - States and transitions

**Petri Nets** - Tokens, places, transitions, arcs, non-deterministic

**Lambda Calculus** - Function binding & substitution

**Universal Turing Machine** - Capable of any computation

**Turing Complete** - Can simulate a Turing Machine

**High Level** - Strong abstraction from the details of implementation

**Low Level** - Close to computer instruction set architecture, fast and efficient (small memory footprint)

# Paradigms

- Imperitive - Ordered set of instructions with a current state and variables
- Declarative - Statements, relations and rules. Desired result.
    - Not necessarily how to solve the problem, or in what order

# Functional Programming

Mathematical functions are the primary (or only) element

- Computation is the evaluation of functions 
- No state
- No variables
- Haskell

## Haskell
> Elegant and concise

Glasgow haskell compiler

[Learn You a Haskell for Practice](https://learnyouahaskell.com)

**High Level** - Strong abstraction / Human readable

**Low Level** - Close to machine code / Less readable

**Everything is a function**

- Computation is a matter of evaluating a function
- Function -> Maps elements in the **domain** to elements in the **co-domain** (the elements used in the co-domain are called the **range**)

**Domain** - Everything that is a legal input to the function
**Co-Domain** - The realm of possible outputs e.g -> XeR
**Range** - All possible outputs in relation to the inputs

- A function is fundamentally a **relation**
- 0 or more arguments
- 1 value (generally speaking, not always true e.g. root(x))
- Value and Arguments can also be a function
- Functions can be combined: **Function Composition**

**First order functions**
> Can take functions as arguments and can return them

**Pure functions**
- No side effects 
- No change state 
    - no state!
    - No loops
- Flexibility in order of computation
    - Great for threads and parallelization

**Referential Transparency**
- Values do not change 
- No Variables

Haskell is __Mostly__ purely functional

Haskell is lazy, meaning nothing is computed until necessary

Infix Operator -> Operation in between operands

In haskell, any function can become infix with the addition of backticks e.g. `(10 \`div\` 5)`

- Functions happen first in order of operations 
- Single dollar sign = 'Compute everything after the sign first'

### Built in functions

min, max, succ, pred, div

### Lists 

[1,2,3,4]

Can be nested -> [[1,2,3],[1,2,3]]

Operators

- : add element to a list
- ++ Concat two lists
- !! Access an element in a list

### List functions

- head -> Very first item
- tail -> The whole list except the first item
- init -> Everything but the last item
- last -> Last item in the list
- More: null minimum reverse maximum take sum drop product elem

### Prelude

The base module of Haskell. All pre-defined functions are part of prelude

### List comprehension

One way to generate a list. Similar to mathematical list notation

eg -> `[x * 2 | x <- [1,2,3], even x] `

`even x` filters the outputs

`x <- [1,2,3]` is the starting values

`x * 2` what to do to the final values

### Tuples

Like lists but fundamentally different

Specific Size - Types do not have to be the same

Tuples used in a function must match: Same length, same types, same order

**fst** - get first element in a tuple

**snd** - get second element in a tuple

## Program Flow

- **Direct Control** goto label / line number
- **Logical Operators** - if, then, else
- **Case/Switch statements**
- Some languages require final statements to end the block of code

### Loops

- Repeat sections of code
- Count - `for`
- Map - `forall`, `foreach`
- Condition - `while`, `dowhile`, `repeat`

### Recursion

- Produces a solution by solving smaller instances of the problem
- Size of problem not defined in code
- Functions call themselves
- Any loop can be written w/ recursion

**Elements of recursion**

**Base case** - Ends the recursion, infinite recursion otherwise

**Recursive Call** - Call the function again. (Makes the problem smaller), may include other processing

### Wrapper functions

- Don't recurse, but set up and call the actual recursive function
- Not necessary, but can hide messy details
    - Counters
    - Intermediate calculations -> accumulator
- Example
```
Foo(){Foo(null);}
Foo(Foo parent) {this.parent = parent};
```
### Tail recursion

Function returns a recursive call. All computation taken care of in the recursive call. Calling function no longer necessary on stack. Allows for compiler optimization in some languages

### Filter 

Returns members of a collection that meet a criteria

- Usually a logical or numerical statement

### Map

- Applying a function or process to every member of a collection
- Famously applied in big data as MapReduce
    - **Maps** an operation to many items
    - **Reduces** those results into a summary output

### Haskell Implementation

Emphasis on which options to use rather than considering flow of execution

### If-Else (Haskell)

```haskell
describeLetter c = 
    if (c >= 'a')
        then "Lower Case"
        else
             "Not Lower case"

```

### Guarded Equations (Haskell)

```haskell
sign x
    | x > 0 = 1
    | x < 0 = (-1)
    | otherwise = 0
```

### Control Case (Haskell)

```haskell
wordTF x = case x of 
    True -> "True"
    False -> "False"
```

### Control: Pattern Matching (Haskell)

Attempted in order from top to bottom

```haskell
wordTF True = "True"
wordTF False = "False"
```

### Recursion (Haskell)

```haskell
fact 1 = 1
fact n a = fact (n-1) (n*a)
```

### Wrapper functions

```haskell
fact = fact' n 1 

fact' 1 = 1
fact' n a = fact (n-1) (n*a)
```

### Let & Where

```haskell
dbl x = let y = x * 2 in y

dbl' x = y where y = x * 2

```

### Aliases: @

```haskell
foo l@(x:xs)
    | even (length l) = [x]
    | otherwise = xs
```

### More List Processing

```haskell
--Parse through list until it hits an odd then stop
takeWhile even [1..10]

--Parse through list, getting every element until it hits an even
dropWhile even [1..10]

--Return 2 new lists with 3 as the delimeter
splitAt 3 [1..10]

```














