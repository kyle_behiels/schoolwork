extern crate rand;

use rand::Rng;

fn main() {
    let mut rng = rand::thread_rng();
    for i in 0..100 { // random bool
        println!("I chose a random number {}", rng.gen_range(0,10));
    }
    
}
