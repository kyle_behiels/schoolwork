let evens x = [i | i <- [0..x] , even i]

let capitalized string = elem (string !! 0) ['A'..'Z'] 

let abbreviate string = (string !! 0) (string !! 1)
main = do
    abbreviate "Hello"
