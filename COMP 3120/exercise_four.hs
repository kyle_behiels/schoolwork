import Data.Time.Calendar
import Data.Time.Format
import Data.Time.Calendar.OrdinalDate
import System.Random

triangle :: (Num a, Enum a) => a -> [a]
triangle n = scanl (\x y -> x+y+1) 0 [0..n];

data Colour = Red | Orange | Yellow | Green | Blue | Indigo | Violet deriving (Eq, Ord, Show)
data Candy = Neegies {
    colour :: Colour,
    age :: Int
} deriving (Eq, Ord, Show)

freshness :: Candy -> Maybe Int
freshness x = if (age x) < 1700 then Just (age x) else Nothing

dayOfMonth :: (t, t1, t2) -> t2
dayOfMonth (_, _, x) = x

days :: Integer -> [Day]
days n = [k | k <- [(fromGregorian n 01 01)..(fromGregorian n 12 31)], snd (mondayStartWeek k) == 5, (dayOfMonth (toGregorian k)) == 13 ]

pick :: IO [Char]
pick = fmap (["bat","dolphin","cat","hippo"] !! ) $ randomRIO (0, 3)