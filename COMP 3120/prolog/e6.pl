difficultfood(bluecheese).
difficultfood(durian).
difficultfood(garlic).
difficultfood(gejang).
difficultfood(injera).
difficultfood(jalapeno).
difficultfood(maatjes).
difficultfood(natto).
difficultfood(slurpee).
difficultfood(stinkytofu).
difficultfood(uni).
difficultfood(marshmallow).
difficultfood(olives).
difficultfood(peanuts).
difficultfood(livewasp).

likes(eri,garlic).
likes(eri,jalapeno).
likes(eri,slurpee).
likes(hamed,uni).
likes(phil,bluecheese).
likes(phil,garlic).
likes(phil,injera).
likes(piper,gejang).
likes(piper,maatjes).
likes(piper,natto).
likes(meredith,marshmallow).
likes(leonardo, olives).
likes(everyonebutme, peanuts).
likes(nobody, livewasp).

likes(jetset, X) :- difficultfood(X).