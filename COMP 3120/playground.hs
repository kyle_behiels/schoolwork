import System.IO

powers a y z = [x ^ z | x <- [a..y]]

recursiveadd x 0 = x
recursiveadd x y = recursiveadd (x + 1) (y - 1)


add x y 
    | y == 0 = x
    | otherwise = add (x + 1) (y - 1)
